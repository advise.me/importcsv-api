import csv
import sys
import numpy as np 

if (len(sys.argv) != 5):
	print('Wrong number of arguments, expected 4, got {}.'.format(len(sys.argv)-1))
	print('Usage : python parsenodes.py [filename: proba.csv] [filename: speed.csv] [PP: 0.2 || 0.025] [threshold: 0.1 || 0.0003 || 0]')
else:
	input_file = sys.argv[1]
	speed_file = sys.argv[2]
	PP = float(sys.argv[3])
	threshold = float(sys.argv[4])
	all_nodes = np.zeros(shape=(180*int(1/PP),360*int(1/PP)),dtype=bool)
	f = open('nodes.csv','w')
	f.write('nodeId:ID,probability,latitude:FLOAT,longitude:FLOAT,speed:FLOAT\n');
	speed =  []
	with open(speed_file) as csvfile:
		reader = csv.reader((line.replace('\0','') for line in csvfile),delimiter=',')
		xlimits = 90
		i = 0
		for row in reader:
			#print('speed: latitude => {:.4f} '.format(xlimits-i*PP))
			tmp = []
			for col in row:
				tmp.append(col)
			i = i + 1
			speed.append(tmp)
	with open(input_file) as csvfile:
		reader = csv.reader((line.replace('\0','') for line in csvfile),delimiter=',')
		csvspeed = open(speed_file,'rb')
		speedreader = csv.reader((line.replace('\0','') for line in csvspeed),delimiter=',')
		xlimits = 90
		ylimits = -180
		i = 0
		j = 0
		id = 1
		for row in reader:
			#print('latitude => {:.4f} '.format(xlimits-i*PP))
			tmp = []
			for col in row:
				if(float(col) > threshold):
					f.write('{0},{1},{2},{3},{4}\n'.format(id,col,xlimits-i*PP,ylimits+j*PP,speed[i][j]))
					tmp.append(True)
				else:
					tmp.append(False)
				id = id + 1
				j = j + 1
			all_nodes[i] = tmp
			i = i +1
			j = 0
	f.close()
	np.save('nodes.npy',all_nodes)
