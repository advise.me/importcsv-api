Import csv into your local neo4j db

Usage : 

python parsenodes.py 
This will generate a nodes.csv containing informations about all nodes in the csv
Make sure to change the PP variable if you are using csv with precision != 1/5

python parserelations.py
This will generate a relationships.csv containing info about all relations in the csv (bidirectional)

Create new database from neo4j desktop first !

You will need to copy those files (nodes.csv and relationships.csv) into your $neo4j_database_home\import
 => something like this : Neo4j Desktop\Application\neo4jDatabases\database-****\installation-*\import

Then navigate to $neo4j_home/bin and run the command : 
neo4j-admin.bat import --id-type integer --nodes:Noeuds ../import/nodes.csv --relationships:ConnectedTo ../import/relationships.csv

This will populate your db

start your neo4j server then navigate to localhost:7474 you should be able to see your nodes and relations ! 

Have fun ! 

Work in progress :
	-> Relationships optimal cost