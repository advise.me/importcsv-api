import csv
import sys
from math import radians, cos, sin, asin, sqrt

def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    r = 6371 # Radius of earth in kilometers. Use 3956 for miles
    return c * r

def get_weight(long1,lat1,long2,lat2,prob1,prob2,speed1,speed2):
	distance = haversine(long1,lat1,long2,lat2)
	#if(prob1 > 0.95):
	#	prob1 = 0.2
	#if(prob2 > 0.95):
	#	prob2 = 0.2
	prob = (prob1 + prob2)/2
	speed = (float(speed1) + float(speed2))*1.852/2
	try:
		weight = distance / (speed)
	except ZeroDivisionError:
		weight = distance * 1000
	return weight / prob

if(len(sys.argv) != 5):
	print('Wrong number of arguments, expected 4, got {}.'.format(len(sys.argv)-1))
	print('Usage : python parserelations.py [filename: proba.csv] [filename: speed.csv] [PP: 0.2 || 0.025] [threshold: 0.1 || 0.0003 || 0]')
else:
	input_file = sys.argv[1]
	speed_file = sys.argv[2]
	PP = float(sys.argv[3])
	threshold = float(sys.argv[4])
	
	matrix = []
	line = []
	f = open('relationships.csv','w')
	f.write(':START_ID,cost,:END_ID\n');
	
	speed =  []
	with open(speed_file) as csvfile:
		reader = csv.reader((line.replace('\0','') for line in csvfile),delimiter=',')
		xlimits = 90
		i = 0
		for row in reader:
			#print('speed: latitude => {:.4f} '.format(xlimits-i*PP))
			tmp = []
			for col in row:
				tmp.append(col)
			i = i + 1
			speed.append(tmp)
	
	with open(input_file) as csvfile:
		reader = csv.reader((line.replace('\0','') for line in csvfile),delimiter=',')
		id = 1
		i = 0
		j = 0
		xlimits = 90 
		ylimits = -180
		for row in reader:
			#print('latitude => {:.4f} '.format(xlimits-i*PP))
			j = 0
			for col in row:
				if(float(col) > threshold):
					col = float(col)
					if(i == 0 and j == 0):  #Premiere ligne Premiere colonne
						#Create a node 
						lat = xlimits-i*PP
						long = ylimits+j*PP
						
						line.append((id,col,long,lat,speed[i][j]))
					elif(i == 0):  #Prmeiere ligne Colonne Lambda
						#Create a node
						lat = xlimits-i*PP
						long = ylimits+j*PP
						line.append((id,col,long,lat,speed[i][j]))
						#Ajout de la relationship
						n2 = line[j-1]
						if(n2 is not None):
							f.write('{0},{1},{2}\n'.format(n2[0], get_weight(n2[2],n2[3],long,lat,n2[1],col,n2[4],speed[i][j]), id))
							f.write('{0},{1},{2}\n'.format(id, get_weight(long,lat,n2[2],n2[3],col,n2[1],speed[i][j],n2[4]), n2[0]))
							#n1.relationships.create("Connect To", n2)
					elif(j == 0):# Ligne Lambda Premiere colonne
						#Create a node 
						lat = xlimits-i*PP
						long = ylimits+j*PP
						line.append((id,col,long,lat,speed[i][j]))
						#Ajout de la relationship
						n2 = matrix[i-1][j]
						if(n2 is not None):
							f.write('{0},{1},{2}\n'.format(n2[0], get_weight(n2[2],n2[3],long,lat,n2[1],col,n2[4],speed[i][j]), id))
							f.write('{0},{1},{2}\n'.format(id, get_weight(long,lat,n2[2],n2[3],col,n2[1],speed[i][j],n2[4]), n2[0]))
							#n1.relationships.create("Connect To", n2)
						n2 = matrix[i-1][j+1]
						if(n2 is not None):
							f.write('{0},{1},{2}\n'.format(n2[0], get_weight(n2[2],n2[3],long,lat,n2[1],col,n2[4],speed[i][j]), id))
							f.write('{0},{1},{2}\n'.format(id, get_weight(long,lat,n2[2],n2[3],col,n2[1],speed[i][j],n2[4]), n2[0]))
							#n1.relationships.create("Connect To", n2)
					else:  #Ligne Lambda Colonne Lambda
						#Create a node 
						lat = xlimits-i*PP
						long = ylimits+j*PP
						line.append((id,col,long,lat,speed[i][j]))
						#Ajout de la relationship
						n2 = line[j-1]
						if(n2 is not None):
							f.write('{0},{1},{2}\n'.format(n2[0], get_weight(n2[2],n2[3],long,lat,n2[1],col,n2[4],speed[i][j]), id))
							f.write('{0},{1},{2}\n'.format(id, get_weight(long,lat,n2[2],n2[3],col,n2[1],speed[i][j],n2[4]), n2[0]))
							#n1.relationships.create("Connect To", n2)
						n2 = matrix[i-1][j]
						if(n2 is not None):
							f.write('{0},{1},{2}\n'.format(n2[0], get_weight(n2[2],n2[3],long,lat,n2[1],col,n2[4],speed[i][j]), id))
							f.write('{0},{1},{2}\n'.format(id, get_weight(long,lat,n2[2],n2[3],col,n2[1],speed[i][j],n2[4]), n2[0]))
							#n1.relationships.create("Connect To", n2)
						n2 = matrix[i-1][j-1]
						if(n2 is not None):
							f.write('{0},{1},{2}\n'.format(n2[0], get_weight(n2[2],n2[3],long,lat,n2[1],col,n2[4],speed[i][j]), id))
							f.write('{0},{1},{2}\n'.format(id, get_weight(long,lat,n2[2],n2[3],col,n2[1],speed[i][j],n2[4]), n2[0]))
							#n1.relationships.create("Connect To", n2)
						try:
							n2 = matrix[i-1][j+1]
							if(n2 is not None):
								f.write('{0},{1},{2}\n'.format(n2[0], get_weight(n2[2],n2[3],long,lat,n2[1],col,n2[4],speed[i][j]), id))
								f.write('{0},{1},{2}\n'.format(id, get_weight(long,lat,n2[2],n2[3],col,n2[1],speed[i][j],n2[4]), n2[0]))
								#n1.relationships.create("Connect To", n2)
						except IndexError:
							pass 
				else:
					line.append(None)
				id = id + 1
				j = j + 1
			n1 = line[1]
			n2 = line[j-1]
			if(n1 is not None and n2 is not None):
				f.write('{0},{1},{2}\n'.format(n2[0], get_weight(n2[2],n2[3],n1[2],n1[3],n2[1],n1[1],n2[4],n1[4]), n1[0]))
				f.write('{0},{1},{2}\n'.format(n1[0], get_weight(n1[2],n1[3],n2[2],n2[3],n1[1],n2[1],n1[4],n2[4]), n2[0]))
			matrix.append(line)
			line = []
			i = i +1
	f.close()

